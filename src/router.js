import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/prestador',
      name: 'prestador',
      // route level code-splitting
      // this generates a separate chunk (rider.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import( './views/Prestador.vue')
    }
    , {
      path: '/rider',
      name: 'rider',
      component: () => import( './views/Rider.vue')
    }
    , {
      path: '/requisitos'
      , component: () => import('./views/Requisitos.vue')
    }
    , {
      path: '/distribuidor'
      , component: () => import('./views/Suplidor.vue')
    }
    , {
      path: '/regiones'
      , component: () => import('./views/Regiones.vue')
    }
    , {
      path: '/empresarial'
      , component: () => import('./views/Empresarial.vue')
    }
    , {
      path: '/usuario'
      , component: () => import('./views/Usuario.vue')
    }
    , {
      path: '/nosotros'
      , component: () => import('./views/About.vue')

    }
    , {
      path: '/carreras'
      , component: () => import('./views/Carreras.vue')

    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
